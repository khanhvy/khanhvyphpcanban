<!DOCTYPE html>
<html>
<head>
	<title>Trang chu</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="index_style.css">
	<link rel="shortcut icon" href="img/logo.png" type="image/vnd.microsoft.icon" id="favicon" />
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/fontawesome-all.css">
	<link rel="stylesheet" href="css/fontawesome-all.min.css">
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body >
<div class="header">
	<div class="container">
	<img src="./img/LOGO_DTDH.png" style="width: 100%">
	</div>
</div>
<div class="container">
  <h3>Tự chủ - Đổi mới - Chất lượng cao</h3>
  <ul class="nav nav-tabs">
    <li class="active"><a href="trangchu.php">Trang chủ</a></li>
    <li><a href="khoa.php">Khoa</a></li>
    <li><a href="monhoc.php">Môn học</a></li>
    <li><a href="sinhvien1.php">Sinh viên</a></li>
    <li><a href="khoa_creat.php">Thêm mới khoa</a></li>
    <li><a href="sinhvien_creat.php">Thêm mới sinh viên</a></li>
    <li><a href="monhoc_creat.php">Thêm mới môn học</a></li>
  </ul>
  <br>
  <p><strong>New:</strong> Trường Đại học Mỏ - Địa chất tổ chức Tập huấn kỹ năng viết CV và trả lời phỏng vấn xin việc bằng tiếng Anh</p>
</div>
<?php 
// Gọi file kết nối CSDL
require('connect.php');

/**
 * Lấy nhiều bản ghi
 */

$sql = "SELECT sinhvien.* , khoa.ten_khoa FROM sinhvien INNER JOIN khoa ON sinhvien.makhoa = khoa.makhoa ";
$query = $db->query($sql);
//MYSQLI_ASSOC đưa về dạng mảng kết hợp: Associative Array
$result = $query->fetch_all(MYSQLI_ASSOC);
?>

<div class="container">
  <h2>Hover Rows</h2>
  <p>The .table-hover class enables a hover state on table rows:</p>            
  <table class="table table-hover">
    <thead>
      <tr>
        <td>STT</td>
        <th>Mã sinh viên</th>
        <th>Tên sinh viên</th>
        <th>Ngày sinh</th>
        <th>Giới tính</th>
        <th>Địa chỉ</th>
        <th>Email</th>
        <th>Mã khoa</th>
        <th>Tên khoa</th>
      </tr>
    </thead>
    <tbody>
    	<?php 
      $stt = 1;
		if (count($result) > 0) :
			foreach ($result as $sv) :
		?>
      <tr>
        <td><?php echo $stt++ ;?></td>
        <td><?php echo $sv['masv'];?></td>
		<td><?php echo $sv['hoten'];?></td>
        <td><?php echo $sv['ngaysinh'];?></td>
        <td><?php if($sv['gioitinh'] == 1) echo 'Nam'; else echo 'Nữ';?>
        </td>
        <td><?php echo $sv['diachi'];?></td>
        <td><?php echo $sv['email'];?></td>
        <td><?php echo $sv['makhoa'];?></td>
        <td><?php echo $sv['ten_khoa'] ?></td>
      </tr>
      <?php
			endforeach;
		endif; 
		?>
    </tbody>
  </table>
</div>

</body>
</html>
<?php

/**
 * Lấy 1 bản ghi
 */

/*$sql = "SELECT * FROM sinhvien LIMIT 1";
$query = $db->query($sql);
$result = $query->fetch_assoc();
print_r($result);

/**
 * Đếm số bản ghi
 */

?>