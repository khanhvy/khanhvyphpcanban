<?php 
require('connect.php');
include("header.php");
$sql = "SELECT * FROM mon_hoc";
$query = $db->query($sql);
$monhoc = $query->fetch_all(MYSQLI_ASSOC);

$sql = "SELECT * FROM sinhvien";
$query = $db->query($sql);
$sinhvien = $query->fetch_all(MYSQLI_ASSOC);
$errors = [];
$isCreted = 0;
if(isset($_POST['submit'])){
	if(!isset($_POST['mamonhoc']) || $_POST['mamonhoc'] == ''){
		$errors[] = 'Chọn mã môn học';
	}
	if(!isset($_POST['masv']) || $_POST['masv'] == ''){
		$errors[] = 'Chọn mã sinh viên';
	}
	if(!isset($_POST['diem']) || $_POST['diem'] == ''){
		$errors[] = 'Vui lòng nhạp điểm';
	}
	if(count($errors) == 0){
		$mamonhoc = $_POST['mamonhoc'];
		$masv = $_POST['masv'];
		$diem = $_POST['diem'];
		$sql = "INSERT INTO ketqua(diem,masv,mamonhoc) VALUES ('".$diem."','".$masv."','".$mamonhoc."')";
		$query = $db->query($sql);
		if($query){
			$isCreted = 1;
		}else{
			$errors[] = 'không thể thêm điểm';
		}
	}
}
?>
<section>
	<div class="container">
		<div class="message">
			<?php 
			if (count($errors) > 0) :
				for ($i = 0; $i < count($errors); $i++) :
					?>
					<p class="error" style="color:red"><?php echo $errors[$i];?></p>
					<?php 
				endfor;
			endif;
			?>
			<?php if ($isCreted == 1) : ?>
				<p class="success" style="color:green">Thêm thành công!</p>
			<?php endif;?>
		</div>
		<form action="" method="POST" accept-charset="utf-8">
			<div> Điểm :
				<input type="number" name="diem" value="<?php if(isset($_POST['submit']) && ($_POST['diem']) != '') echo $_POST['diem']; ?>">	
			</div>
			<div>Mã môn học <span style="color: red">(*)</span>: <select name="mamonhoc"><option value="">-- Chọn --</option>
				<?php if(!is_null($monhoc) && count($monhoc) > 0) : foreach ($monhoc as $mh):?>
					<option value="<?php echo $mh['mamonhoc'];?>"><?php echo $mh['tenmonhoc']; ?></option>
					<?php 
				endforeach;
			endif;
			?>
		</select></div>
		<div> Mã sinh viên 
			<input list="browsers" name="masv">
			<datalist id="browsers">
				<option value="">-- Chọn --</option>
				<?php if(!is_null($sinhvien) && count($sinhvien) > 0) : foreach ($sinhvien as $sv):?>
					<option value="<?php echo $sv['masv'];?>"><?php echo $sv['hoten']; ?></option>
					<?php 
				endforeach;
			endif;
			?>
		</datalist>
		
	</div>	
	
	<div>
		<input type="submit" name="submit" value="Thêm mới điểm">
	</div>
</form>
</div>
</section>