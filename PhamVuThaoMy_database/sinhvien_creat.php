<?php 
require('connect.php');
include("header.php");
 $sql = "SELECT * FROM khoa";
$query = $db->query($sql);
$khoa = $query->fetch_all(MYSQLI_ASSOC);
$errors = [];
$isCreted = 0;
if(isset($_POST['submit'])){
	if(!isset($_POST['masv']) || $_POST['masv'] == ''){
		$errors[] = 'vui lòng nhập ms sinh viên ';
	}
	if(!isset($_POST['hoten']) || $_POST['hoten'] == ''){
		$errors[] = 'vui lòng nhập họ tên ';
	}
	if(!isset($_POST['ngaysinh']) || $_POST['ngaysinh'] == ''){
		$errors[] = 'vui lòng nhập ngày sinh';
	}
	if(!isset($_POST['email']) || $_POST['email'] == ''){
		$errors[] = 'vui lòng nhập email ';
	}
	if(count($errors) == 0){
		$masv = trim($_POST['masv']);
		$hoten = trim($_POST['hoten']);
		$ngaysinh = $_POST['ngaysinh'];
		$gioitinh = $_POST['gioitinh'];
		$diachi = trim($_POST['diachi']);
		$email = trim($_POST['email']);
		$makhoa = $_POST['makhoa'];
		$sql = "SELECT * FROM sinhvien WHERE masv = '".$masv."' LIMIT 1";
		$query = $db->query($sql);
		$result = $query->fetch_assoc();
		if(!is_null($result)){
			$errors[] = 'Mã sinh viên này đã tồn tại !';
		}else{
			$sql = "INSERT INTO sinhvien(masv,hoten,ngaysinh,gioitinh,diachi,email,makhoa) VALUES ('".$masv."', '".$hoten."','".$ngaysinh."','".$gioitinh."','".$diachi."','".$email."','".$makhoa."')";
			$query = $db->query($sql);
			if($query){
				$isCreted = 1;
			}else{
				$errors[] = 'Không thể thêm sv';
			}
		}
	}
}
?>
<section>
	<div class="container">
		<div class="message">
			<?php 
			if (count($errors) > 0) :
				for ($i = 0; $i < count($errors); $i++) :
			?>
			<p class="error" style="color:red"><?php echo $errors[$i];?></p>
			<?php 
				endfor;
			endif;
			?>
			<?php if ($isCreted == 1) : ?>
			<p class="success" style="color:green">Thêm sinh viên thành công!</p>
			<?php endif;?>
		</div>
	
	<form action="" method="POST" accept-charset="utf-8">
		<div> Mã sinh viên <span style="color:red">(*)</span> : <input type="text" name="masv"  value="<?php if(isset($_POST['submit']) && ($_POST['hoten']) != '') echo $_POST['hoten']; ?>">
		</div>
		<div> Tên sinh viên <span style="color: red">(*)</span> : <input type="text" name="hoten" value="<?php if(isset($_POST['submit']) && $_POST['hoten'] != '') echo $_POST['hoten']; ?>">
		</div>
		<div> Ngày sinh <span style="color : red">(*)</span> : <input type="text" name="ngaysinh" placeholder="yyy-mm-dd" value="<?php if(isset($_POST['submit']) && ($_POST['ngaysinh']) != '') echo $_POST['ngaysinh']; ?>">
		</div>
		<div> Giới tính <span style="color : red">(*)</span>: 
			Nam <input type="radio" name="gioitinh" value="1">
			Nữ <input type="radio" name="gioitinh" value="2">
		</div>
		<div> Địa chỉ : <input type="text" name="diachi" value="<?php if(isset($_POST['submit']) && $_POST['diachi'] != '') echo $_POST['diachi']; ?>">
		</div>
		<div> Email <input type="text" name="email" placeholder="abc@gmail.com" value="<?php if(isset($_POST['submit']) && $_POST['email'] != '') echo $_POST['email']; ?>">
		</div>
		<div>
			Khoa<span style="color: red">(*)</span>: <select name="makhoa"><option value="">--Chọn--</option>
			<?php
			if(!is_null($khoa) && count($khoa) > 0):
				foreach ($khoa as $item):?>
			 <option value="<?php echo $item['makhoa']; ?>"><?php echo $item['ten_khoa'];?></option>
			 <?php 
			endforeach;
		endif;
			 ?>
		</select>
			
		</div>
		<div>
			<input type="submit" name="submit" value="Thêm mới">
		</div>
		
	</form>
	</div>
</section>