<?php 
// Gọi file kết nối CSDL
require('connect.php');

/**
 * Lấy nhiều bản ghi
 */


?>

<!DOCTYPE html>
<html>
<head>
	<title>Trang chu</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="index_style.css">
	<link rel="shortcut icon" href="img/logo.png" type="image/vnd.microsoft.icon" id="favicon" />
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/fontawesome-all.css">
	<link rel="stylesheet" href="css/fontawesome-all.min.css">
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body >
<div class="header">
	<div class="container">
	<img src="./img/LOGO_DTDH.png" style="width: 100%">
	</div>
</div>
<div class="container">
  <h3>Tự chủ - Đổi mới - Chất lượng cao</h3>
  <ul class="nav nav-tabs">
    <li class="active"><a href="trangchu.php">Trang chủ</a></li>
    <li><a href="khoa.php">Khoa</a></li>
    <li><a href="monhoc.php">Môn học</a></li>
    <li><a href="sinhvien1.php">Sinh viên</a></li>
    <li><a href="khoa_creat.php">Thêm mới khoa</a></li>
    <li><a href="sinhvien_creat.php">Thêm mới sinh viên</a></li>
    <li><a href="monhoc_creat.php">Thêm mới môn học</a></li>
  </ul>
  <br>
  <p><strong>New:</strong> Trường Đại học Mỏ - Địa chất tổ chức Tập huấn kỹ năng viết CV và trả lời phỏng vấn xin việc bằng tiếng Anh</p>
</div>

<div class="gt">	
	<div class="container">
  <h2><a href="#">TIN MỚI NHẤT</a></h2>  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="./img/htkh_trac dia.jpg" alt="Los Angeles" style="width:100%;">
      </div>

      <div class="item">
        <img src="./img/openweb.jpg" alt="Chicago" style="width:100%;">
      </div>
    
      <div class="item">
        <img src="./img/qt2.jpg" alt="New york" style="width:100%;">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

</div>
<div class="container">
  
  <h2>Linked Items With Contextual Classes</h2>
  <p>Move the mouse over the linked items to see the hover effect:</p>
  <div class="list-group">
    <a href="khoa.php" class="list-group-item list-group-item-success">Tổng số khoa <span class="badge"><?php $sql = "SELECT COUNT(*) FROM khoa"; 
    $query = $db->query($sql);
    $result = $query->fetch_row();
    echo $result[0]; ?></span></a>
    <a href="sinhvien.php" class="list-group-item list-group-item-info">Tổng số Sinh Viên <span class="badge"><?php $sql = "SELECT COUNT(*) FROM sinhvien";
$query = $db->query($sql);
$result = $query->fetch_row();
echo $result[0];?></span></a>
    <a href="monhoc.php" class="list-group-item list-group-item-warning">Tổng số môn học <span class="badge"><?php $sql = "SELECT COUNT(*) FROM mon_hoc";
$query = $db->query($sql);
$monhoc = $query->fetch_row();
echo $monhoc[0];
$db->close(); ?></span></a>
    
  </div>
</div>


</body>
</html>