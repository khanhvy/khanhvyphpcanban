<?php

require('connect.php');
include("header.php");

$errors = [];
$isCreated = 0;
if (isset($_POST['submit'])) {
	if (!isset($_POST['makhoa']) || $_POST['makhoa'] == '') {
		$errors[] = 'Vui lòng nhập Mã Khoa';
	}

	if (!isset($_POST['ten_khoa']) || $_POST['ten_khoa'] == '') {
		$errors[] = 'Vui lòng nhập Tên Khoa';
	}

	if (count($errors) == 0) {
		//Xu ly them vao CSDL
		$makhoa = trim($_POST['makhoa']);
		$tenkhoa = trim($_POST['ten_khoa']);

		//kiem tra xem ma khoa duoc nhap vao da ton tai hay chua
		$sql = "SELECT * FROM khoa WHERE makhoa = '". $makhoa ."' LIMIT 1";
		$query = $db->query($sql);
		$result = $query->fetch_assoc();
		if (!is_null($result)) {
			$errors[] = 'Mã khoa này đã tồn tại. Vui lòng nhập Mã khác!';
		} else {
			//Them vao CSDL
			$sql = "INSERT INTO khoa (makhoa, ten_khoa) VALUES ('". $makhoa ."', '". $tenkhoa ."')";
			$query = $db->query($sql);
			if ($query) {
				$isCreated = 1;
			} else {
				$errors[] = 'Không thể thêm khoa!';
			}
		}

	}

}

?>
<section>
	<div class="container">
		
		<div class="message">
			<?php 
			if (count($errors) > 0) :
				for ($i = 0; $i < count($errors); $i++) :
			?>
			<p class="error" style="color:red"><?php echo $errors[$i];?></p>
			<?php 
				endfor;
			endif;
			?>
			<?php if ($isCreated == 1) : ?>
			<p class="success" style="color:green">Thêm khoa thành công!</p>
			<?php endif;?>
		</div>
		

		<form action="" method="POST" accept-charset="utf-8">
			<div>Mã khoa <span style="color:red">(*)</span>: <input type="text" name="makhoa" value=""></div>
			<div>Tên khoa <span style="color:red">(*)</span>: <input type="text" name="ten_khoa" value=""></div>
			<div>
				<input type="submit" name="submit" value="Thêm mới">
			</div>
		</form>
	</div>
</section>



	
	


