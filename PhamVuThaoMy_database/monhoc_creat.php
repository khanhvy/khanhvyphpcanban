<?php 
require('connect.php');
include("header.php");
$errors = [];
$isCreated = 0;
if(isset($_POST['submit'])){
	if(!isset($_POST['mamonhoc']) || $_POST['mamonhoc'] == ''){
		$errors[] = 'Vui lòng nhập mã môn học';
	}
	if(!isset($_POST['tenmonhoc']) || $_POST['tenmonhoc'] == ''){
		$errors[] = 'Vui lòng nhập tên môn học';
	}
	if(count($errors) == 0){
		$mamonhoc = trim($_POST['mamonhoc']);
		$tenmonhoc = trim($_POST['tenmonhoc']);
		$sql = "SELECT * FROM mon_hoc WHERE mamonhoc = '".$mamonhoc."' LIMIT 1";
		$query = $db->query($sql);
		$result = $query->fetch_assoc();
		if(!is_null($result)){
			$errors[] = 'Mã môn này đã tồn tại ';
		}else{
			$sql = "INSERT INTO mon_hoc(mamonhoc,tenmonhoc) VALUES('".$mamonhoc."','".$tenmonhoc."')";
			$query = $db -> query($sql);
			if($query){
				$isCreated = 1;
			}else{
				$errors[] = "Không thể thêm môn";
			}
		}
	}
}
?>
<section>
	<div class="container">
		<div class="message">
			<?php 
			if (count($errors) > 0) :
				for ($i = 0; $i < count($errors); $i++) :
			?>
			<p class="error" style="color:red"><?php echo $errors[$i];?></p>
			<?php 
				endfor;
			endif;
			?>
			<?php if ($isCreated == 1) : ?>
			<p class="success" style="color:green">Thêm khoa thành công!</p>
			<?php endif;?>
		</div>
	
	<form action="" method="POST" accept-charset="utf-8">
		<div>Mã môn học <span style="color: red">(*)</span> : <input type="text" name="mamonhoc" value=""></div>
		<div>Tên môn học <span style="color: red">(*)</span>: <input type="text" name="tenmonhoc" value=""></div>
		<div><input type="submit" name="submit" value="Thêm mới "></div>
		
	</form>
	</div>
	
</section>