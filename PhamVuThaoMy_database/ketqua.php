<?php
require('connect.php');
include("header.php");
$sql = "SELECT ketqua.diem, sinhvien.hoten, sinhvien.masv, mon_hoc.mamonhoc, mon_hoc.tenmonhoc FROM ketqua INNER JOIN sinhvien ON ketqua.masv=sinhvien.masv INNER JOIN mon_hoc ON ketqua.mamonhoc=mon_hoc.mamonhoc";
$query = $db->query($sql);
$result = $query->fetch_all(MYSQLI_ASSOC);
?>
<div class="container">
  <h2>Hover Rows</h2>
  <p>The .table-hover class enables a hover state on table rows:</p>            
  <table class="table table-hover">
    <thead>
      <tr>
      	
      	<th>Họ tên</th>
      	<th>Mã sinh viên</th>
        <th>Mã môn học</th>
        <th>Tên môn học</th>
        <th>Điểm</th>
      </tr>
    </thead>
    <tbody>
      <?php 
    if (count($result) > 0) :
      foreach ($result as $kq) :
    ?>
      <tr>
        
        <td><?php echo $kq['hoten']; ?></td>
    <td><?php echo $kq['masv'];?></td>
    <td><?php echo $kq['mamonhoc'];?></td>
    <td><?php echo $kq['tenmonhoc']; ?></td>
        <td><?php echo $kq['diem'];?></td>
      </tr>
      <?php
      endforeach;
    endif; 
    ?>
    </tbody>
  </table>
</div>