
<?php 
// Gọi file kết nối CSDL
require('connect.php');
include("header.php");
/**
 * Lấy nhiều bản ghi
 */

$sql = "SELECT * FROM sinhvien ";
$query = $db->query($sql);
//MYSQLI_ASSOC đưa về dạng mảng kết hợp: Associative Array
$result = $query->fetch_all(MYSQLI_ASSOC);
?>

<div class="container">
  <h2>Hover Rows</h2>
  <p>The .table-hover class enables a hover state on table rows:</p>            
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Mã sinh viên</th>
        <th>Tên sinh viên</th>
        <th>Ngày sinh</th>
        <th>Giới tính</th>
        <th>Địa chỉ</th>
        <th>Email</th>
        <th>Mã khoa</th>
      </tr>
    </thead>
    <tbody>
    	<?php 
		if (count($result) > 0) :
			foreach ($result as $sv) :
		?>
      <tr>
        <td><?php echo $sv['masv'];?></td>
		<td><?php echo $sv['hoten'];?></td>
        <td><?php echo $sv['ngaysinh'];?></td>
        <td><?php echo $sv['gioitinh'];?></td>
        <td><?php echo $sv['diachi'];?></td>
        <td><?php echo $sv['email'];?></td>
        <td><?php echo $sv['makhoa'];?></td>
      </tr>
      <?php
			endforeach;
		endif; 
		?>
    </tbody>
  </table>
</div>

</body>
</html>
<?php

/**
 * Lấy 1 bản ghi
 */

/*$sql = "SELECT * FROM sinhvien LIMIT 1";
$query = $db->query($sql);
$result = $query->fetch_assoc();
print_r($result);

/**
 * Đếm số bản ghi
 */

?>