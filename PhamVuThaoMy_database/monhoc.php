<?php 
require('connect.php');
include("header.php");
$sql = "SELECT * FROM mon_hoc";
$query = $db->query($sql);
//MYSQLI_ASSOC đưa về dạng mảng kết hợp: Associative Array
$result = $query->fetch_all(MYSQLI_ASSOC);
?>


<div class="container">
  <h2>Hover Rows</h2>
  <p>The .table-hover class enables a hover state on table rows:</p>            
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Mã môn học</th>
        <th>Tên môn học</th>
        
      </tr>
    </thead>
    <tbody>
      <?php 
    if (count($result) > 0) :
      foreach ($result as $mh) :
    ?>
      <tr>
        <td><?php echo $mh['mamonhoc'];?></td>
    <td><?php echo $mh['tenmonhoc'];?></td>
        
      </tr>
      <?php
      endforeach;
    endif; 
    ?>
    </tbody>
  </table>
</div>

</body>
</html>