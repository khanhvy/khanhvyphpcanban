<?php

$rootPath = dirname(__FILE__);
define('ROOT_PATH', $rootPath);

require $rootPath . DIRECTORY_SEPARATOR . 'config.php';

define('CONTROLLER_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR);

define('MODEL_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR);

define('VIEW_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR);

define('SYSTEM_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'system' . DIRECTORY_SEPARATOR);

define('HELPER_PATH', ROOT_PATH . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR);


$controller = isset($_GET['c']) ? $_GET['c'] : 'index';
$method = isset($_GET['m']) ? $_GET['m'] : 'index';

$className = ucfirst($controller) . 'Controller';



require_once SYSTEM_PATH . "Database.php";
require_once HELPER_PATH . "helper.php";

if (file_exists(CONTROLLER_PATH . $className . '.php')) {
	require(CONTROLLER_PATH . $className . '.php');
	$c = new $className();
	$c->$method();
}