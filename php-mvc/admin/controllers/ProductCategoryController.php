<?php 
require MODEL_PATH . 'ProductCategory.php';
class ProductCategoryController {

	protected  $product_categoryModel ;

	public function __construct()
	{
		$this->product_categoryModel = new ProductCategory();

	}
	public function index()
	{
		$data = [];
		$where = '';
		if (isset($_GET['name']) && $_GET['name'] != '') {
			$where = "name LIKE '%" .trim($_GET['name']) . "%'";
		}
	$product_categories = $this->product_categoryModel->getProductCategories($where);
	$data['product_categories'] = $product_categories;

	return view('product_categories.index',$data);
	}
	
	public function update() 
	{
		$data = $errors = [];

		$id = isset($_GET['id']) ? (int) $_GET['id'] : 0;

		if ($id == 0) {
			redirect('index.php?c=productCategory');
		}

		$where = 'id = ' . $id;
		$product_category = $this->$product_categoryModel->getProductCategory($where);
		$data['product_category'] = $product_category;

		if (isset($_POST['submit'])) {
			if (!isset($_POST['name']) || $_POST['name'] == '') {
				$errors[] = 'Vui lòng nhập tên thương hiệu';
			}

			if (!isset($_POST['slug']) || $_POST['slug'] == '') {
				$errors[] = 'Vui lòng nhập đường dẫn tĩnh';
			}

			if (count($errors) == 0) {
				$name = trim($_POST['name']);
				$slug = trim($_POST['slug']);
				$status = trim($_POST['status']);
				$product_category = $this->product_categoryModel->editProductCategory($id, $name, $slug, $status);
				if ($product_category) {
					redirect('index.php?c=productCategory&m=index');
				}
			}
		}

		$data['errors'] = $errors;
		return view('product_categories.update', $data);
	}


	public function crate()
	{	
		$data = $errors = [];

		if (isset($_POST['submit'])) {
			if (!isset($_POST['name']) || $_POST['name'] == '') {
				$errors[] = 'Vui lòng nhập tên ';
			}

			if (!isset($_POST['slug']) || $_POST['slug'] == '') {
				$errors[] = 'Vui lòng nhập đường dẫn tĩnh';
			}

			if (count($errors) == 0) {
				$name = trim($_POST['name']);
				$slug = trim($_POST['slug']);
				$status = trim($_POST['status']);
				$product_category = $this->product_categoryModel->addProductCategory($name, $slug, $status);
				if ($product_category) {
					redirect('index.php?c=productCategory&m=index');
				}
			}
		}

		$data['errors'] = $errors;

		return view('product_categories.create', $data);
	}


	public function delete()
	{
		
	}
}
?>