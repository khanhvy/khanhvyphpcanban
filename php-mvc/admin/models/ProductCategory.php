<?php 

class ProductCategory extends Database {

	public function getProductCategories($where = '' , $litmit = 10, $offset = 0, $orderby = '')
	{
		$condition = '';
		if ($where != '') {
			$condition = 'AND ' . $where;
		}
		 $sql = sprintf("SELECT * FROM product_categories WHERE 1=1 %s LIMIT %s OFFSET %s %s",$condition,$litmit,$offset,$orderby);
		 try {
		 	$query = $this->_connect->query($sql);
		 	if ($query) {
		 		return $query-> fetch_all(MYSQLI_ASSOC);
		 	}
		 } catch (Exception $ex) {
		 	die($ex->getMessage());
		 }
		 echo $sql;
		 return null;
	}

	public function getProductCategory($where)
	{
		$condition = '';
		if ($where != '') {
			$condition = 'AND' . $where;
		}

		$sql = sprintf("SELECT * FROM product_categories WHERE 1= 1 %s LIMIT 1",$condition);
		try{
			$query = $this->_connect->query($sql);
			if ($query) {
				return $query->fetch_assoc();
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}

	public function addProductCategory($name, $slug ,$status)
	{
		$sql = "INSERT INTO product_categories(name , slug, status) VALUES ('$name', '$slug' ,'$status')";
		try{
			$query = $this->_connect->query($sql);
			if($query){
				return true;
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}
		return false;
	}

 	
 	public function editProductCategory($id, $name ,$slug , $status)
 	{
 		$sql = "UPDATE product_categories SET name = '$name' , slug = '$slug' , status = $status WHERE id = $id";
 		try {

 			$query = $this->_connect->query($sql);
 			if ($query) {
 				return true;
 			}
 		} catch(Exception $ex) {
 			die($ex ->getMessage());
 		}

 		return false;
 	}

 	public function deleteProductCategory($id)
 	{
 		$sql = "DELETE FROM product_categories WHERE id = $id";
 		try {

 			$query = $this->_connect->query($sql);
 			if ($query) {
 				return true;
 			}
 		} catch (Exception $ex) {
 			die($ex->getMessage());
 		}

 		return false;
 	}
 }
?>