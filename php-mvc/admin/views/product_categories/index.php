<form action="" method="get" accept-charset="utf-8">
	<input type="text" name="name">
	<input type="submit" name="submit" value="Tìm">
</form>

<table border="1">
	<thead>
		<tr>
			<th>STT</th>
			<th>ID</th>
			<th>Tên danh mục sản phẩm </th>
			<th>Trạng thái</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php
		if (count($product_categories) > 0) : 
			$i = 0;
			foreach ($product_categories as $item) :
				$i++;
		?>
		<tr>
			<td><?php echo $i++;?></td>
			<td><?php echo $item['id'];?></td>
			<td><?php echo $item['name'];?></td>
			<td><?php echo displayStatus($item['status']);?></td>
			<td>
				<a href="index.php?c=productCategory&m=update&id=<?php echo $item['id'];?>" title="">Sửa</a>
				<a href="index.php?c=productCategory&m=delete&id=<?php echo $item['id'];?>" title="">Xóa</a>
			</td>
		</tr>
		<?php
			endforeach;
		else: 
		?>
			<tr><td colspan="5">Chưa có bản ghi</td></tr>
		<?php
		endif; 
		?>
	</tbody>
</table>