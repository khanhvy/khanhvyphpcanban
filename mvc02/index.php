
<?php 
require('helpers/helper.php');
$controller = isset($_GET['c']) ? $_GET['c'] : 'index';
$method = isset($_GET['m']) ? $_GET['m'] : 'index';
$className = ucfirst($controller) . 'Controller';
if (file_exists('controllers/' . $className . '.php')) {
	require('controllers/' . $className . '.php');
	$abc = new $className();
	$abc->$method();
}
