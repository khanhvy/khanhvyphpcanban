<?php 
class BrandController {
	public function index()
	{
		$data = [];
		$data['totalBrand'] = 50;
		$data['Brand'] = ['Nice', 'Adidas'];
		return view('brands.brand',$data);
	}
	public function create()
	{
		return view('brands.create');
	}
	public function update()
	{
		return view('brands.update');
	}
	public function delete()
	{
		return view('brands.delete');
	}
}
/*
Tìm hiểu database (system);
Product (trong foder : Modes)
ProductController
-Thuộc tính
-Hàm khỏi tạo 
-Index
-Đoạn require ngay đầy file;
Xem file index.php (Round)
*/
?>
