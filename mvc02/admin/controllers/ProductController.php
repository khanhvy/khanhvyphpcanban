<?php

require 'models' . 'Product.php';

class ProductController {

	protected $productModel;

	public function index($where = '', $limit = 10, $offset = 0, $orderBy = '')
	{
		$this->productModel = new Product();
		$products = $this->productModel->getProducts($where, $limit, $offset, $orderBy);
		$data = [
			'products' => $products,
			'totalRecord' => 50,
		];

		return view('products.index', $data);
	}

	public function create()
	{
		if (isset($_POST['submit'])) {
			$data = $_POST['data'];
		}
		return view('products.create');
	}

	public function update()
	{
		return view('products.update');
	}

	public function delete()
	{
		
	}

}