
<?php 

require 'models/ProductCategory.php';
class ProductCategoryController {

	protected $productCategoryModel;

	public function __construct()
	{
		$this->productCategoryModel = new ProductCategory();
	}
	public function index()

	{
		$data = [];
		$categories = $this->productCategoryModel->getCategories();
		$data['categories'] = $categories;
		return view('product_categorys.index', $data);
	}

	public function create()
	{
		return view('product_categorys.create');
	}

	public function update()
	{
		return view('product_categorys.update');
	}

	public function delete()
	{

	}


}
?>
