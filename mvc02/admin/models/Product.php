<?php

class Product extends Database {

    public function getProducts($where = '', $limit = 10, $offset = 0, $orderBy = '')
    {

        $sql = sprintf("SELECT products.*, product_categories.name as cate_name, brands.name as brand_name FROM products LEFT JOIN product_categories ON products.product_category_id = product_categories.id LEFT JOIN brands ON products.brand_id=brands.id WHERE 1=1 %s %s LIMIT %s OFFSET %s", $where, $orderBy, $limit, $offset);

        $query = $this->_connect->query($sql);
        if ($query) {
            return $query->fetch_all(MYSQLI_ASSOC);
        }

        return null;
    }

    public function getProduct($where = '', $orderBy = '', $offset = 0)
    {

        $sql = sprintf("SELECT products.*, product_categories.name as cate_name, brands.name as brand_name FROM products LEFT JOIN product_categories ON products.product_category_id = product_categories.id LEFT JOIN brands ON products.brand_id=brands.id WHERE 1=1 %s %s LIMIT 1 OFFSET %s", $where, $orderBy, $offset);

        $query = $this->_connect->query($sql);
        if ($query) {
            return $query->fetch_assoc();
        }

        return null;
    }

    public function addProduct($name)
    {
			# code...
    }
}

