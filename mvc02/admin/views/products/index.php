
<?php 
if (isset($products) && count($products) > 0): 
	
global $baseURL;
global $queryString;
global $page;
global $limit;
?>

<section class="show clear">
	<div class="namepage clear">
		<div class="container">
			<h1>Quản lý Sản phẩm</h1>
		</div>
	</div>
	<div class="menubar clear">
		<div class="container">
			<ul>
				<li><a href="index.php?c=product&m=create">Thêm sản phẩm</a></li>
				<li class="message" style="padding-left:5%;">
					<?php if(isset($_SESSION['flash_msg'])) :?>
						<span><?php echo $_SESSION['flash_msg']; unset($_SESSION['flash_msg']);?></span>
					<?php endif;?>
				</li>
			</ul>
		</div>
	</div>
	<div class="tuychinh clear">
		<div class="container">
			<div class="left">
				<span><big>Tổng: <?php echo $totalRecord; ?></big></span>
				<span class="number">
						Hiển thị: 
						<select onchange="window.location.href=this.value">
							<option value="<?php echo 'limit=10';?>">10</option>
							<option value="<?php echo 'limit=20';?>">20</option>
							<option value="<?php echo 'limit=50';?>">50</option>
							<option value="<?php echo 'limit=100';?>">100</option>
						</select>
				</span>
			</div>
			<div class="right">
				<span class="orderby">
					Sắp xếp: 
					<select onchange="window.location.href=this.value">
						<option value="<?php echo 'col=id&by=ASC'; ?>" <?php if (isset($col) && $col=='id' && $by == 'ASC') {echo 'selected';} ?> >id Tăng</option>
						<option value="<?php echo 'col=id&by=DESC'; ?>" <?php if (isset($col) && $col=='id' && $by == 'DESC') {echo 'selected';} ?> >id Giảm</option>
						<option value="<?php echo 'col=name&by=ASC'; ?>" <?php if (isset($col) && $col=='name' && $by == 'ASC') {echo 'selected';} ?> >name Tăng</option>
						<option value="<?php echo 'col=name&by=DESC'; ?>" <?php if (isset($col) && $col=='name' && $by == 'DESC') {echo 'selected';} ?> >name Giảm</option>
					</select>
				</span>
				<span class="finding">
					<form action="" method="get">
						<input id="key" type="text" name="key" class="form" placeholder="từ khóa" value="<?php if(isset($_GET['key'])) {echo $_GET['key'];}?>">
						<input onclick="return testFind();" type="submit" name="find" value="Tìm Kiếm" class="button">
					</form>
				</span>
			</div>
		</div>
	</div>
	<div class="tb-border clear">
		<div class="container">
			<form action="" method="post">
				<table border="1">
					<tr>
						<th colspan="3">Thao tác</th>
						<th>id</th>
						<th>sku</th>
						<th>Tên sản phẩm</th>
						<th>slug</th>
						<th>Giá</th>
						<th>màu sắc</th>
						<th>sizes</th>
						<th>Số lượng</th>
						<th>Thương hiệu</th>
						<th>Danh mục</th>
						<th>Mô tả</th>
						<th>Chi tiết</th>
						<th>Sao</th>
						<th>Mới</th>
						<th>Khuyến mãi</th>
						<th>Nổi bật</th>
						<th>Bán chạy</th>
						<th>Ngày tạo</th>
						<th>Ngày sửa</th>
						<th>meta title</th>
						<th>meta keyword</th>
						<th>meta description</th>
						<th>Trạng thái</th>
					</tr>
					<?php foreach ($products as $product): ?>
						<tr>
							<td class="options"><a href="<?php echo 'index.php?c=product&m=update&id=' . $product['id'];?>">sửa</a></td>
							<td class="options"><a onclick="return confirm('Bạn chắc chắn muốn xóa không?');" href="<?php echo 'index.php?c=product&m=delete&id=' . $product['id'];?>">xóa</a></td>
							<td class='data-show'><?php echo $product['id']; ?></td>
							<td class='data-show'><?php echo $product['sku']; ?></td>
							<td class='data-show'><?php echo $product['name']; ?></td>
							<td class='data-show'><?php echo $product['slug']; ?></td>
							<td class='data-show'><?php echo $product['price']; ?></td>
							<td class='data-show'><?php echo $product['colors']; ?></td>
							<td class='data-show'><?php echo $product['sizes']; ?></td>
							<td class='data-show'><?php echo $product['qty']; ?></td>
							<td class='data-show'><?php echo $product['brand_name']; ?></td>
							<td class='data-show'><?php echo $product['cate_name']; ?></td>
							<td class='data-show'><?php echo $product['description']; ?></td>
							<td class='data-show'><?php echo $product['content']; ?></td>
							<td class='data-show'><?php echo $product['rate']; ?></td>
							<td class='data-show'><?php echo $product['is_new']; ?></td>
							<td class='data-show'><?php echo $product['is_promotion']; ?></td>
							<td class='data-show'><?php echo $product['is_featured']; ?></td>
							<td class='data-show'><?php echo $product['is_sale']; ?></td>
							<td class='data-show'><?php echo $product['created_at']; ?></td>
							<td class='data-show'><?php echo $product['updated_at']; ?></td>
							<td class='data-show'><?php echo $product['meta_title']; ?></td>
							<td class='data-show'><?php echo $product['meta_keyword']; ?></td>
							<td class='data-show'><?php echo $product['meta_description']; ?></td>
							<td class='data-show'><?php echo $product['status']; ?></td>
						</tr>
					<?php endforeach; ?>
				</table>
			</form>
		</div>
	</div>
	<div class="paging clear">
		<div class="container">
			<?php echo paging($baseURL, $queryString, $totalRecord, $page, $limit);?>
		</div>
	</div>
</div>
</section>
<?php else: ?>
	<div>Không có dữ liệu!</div>
<?php endif; ?>

