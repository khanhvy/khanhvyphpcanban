<?php
require('connect.php');
$sql = 'SELECT * FROM product_categories';
$query = $db->query($sql);
$dmsp = $query->fetch_all(MYSQLI_ASSOC);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Danh muc sp</title>
</head>
<body>
<header>
	<div class="header-top">
		<div class="container">
		<div class="logo">
			<img src="https://image.freepik.com/free-vector/abstract-colorful-floral-shape-with-logo_1035-8982.jpg" style="width: 100px">
		</div>
		<div class="sitename">
			<h2>Sell_fine_art</h2>
		</div>
		</div>
	</div>
	<div class="menu">
		<div class="container">
			<ul>
				<?php for($i = 0 ; $i< count($dmsp) ;$i++): ?>
					<li>
						<a href="<?php echo $dmsp[$i]['slug'].'.html' ?>"><?php echo $dmsp[$i]['name']; ?></a>
					</li>
				<?php endfor; ?>
			</ul>
		</div>
	</div>
</header>