<?php
	//Dap an
	$answers = [
		['question_id' => 1, 'option' => 'C'],
		['question_id' => 2, 'option' => 'A'],
		['question_id' => 3, 'option' => 'C'],
		['question_id' => 4, 'option' => 'B'],
		['question_id' => 5, 'option' => 'C'],
		['question_id' => 6, 'option' => 'C'],
		['question_id' => 7, 'option' => 'D'],
		['question_id' => 8, 'option' => 'D']
	];

	//Cac cau hoi
	$questions = [
		[
			'id' => 1,
			'question' => 'Có một đàn vịt, cho biết 1 con đi trước thì có 2 con đi sau, 1 con đi sau thì có 2 con đi trước, 1 con đi giữa thì có 2 con đi 2 bên. Hỏi đàn vịt đó có mấy con?'
		],
		[
			'id' => 2,
			'question' => 'Làm thế nào để qua được câu này?'
		],
		[
			'id' => 3,
			'question' => 'Sở thú bị cháy, con gì chạy ra đầu tiên?'
		],
		[
			'id' => 4,
			'question' => 'Bệnh gì bác sỹ bó tay?'
		],
		[
			'id' => 5,
			'question' => 'Ở Việt Nam, rồng bay ở đâu và đáp ở đâu?'
		],
		[
			'id' => 6,
			'question' => 'Tay cầm cục thịt nắn nắn, tay vỗ mông là đang làm gì?'
		],
		[
			'id' => 7,
			'question' => 'Con gấu trúc ao ước gì mà không bao giờ được?'
		],
		[
			'id' => 8,
			'question' => 'Có 1 đàn chim đậu trên cành, người thợ săn bắn cái rằm. Hỏi chết mấy con?'
		]
	];
	//Cac phuong an tra loi
	$options = [
		[
			'question_id' => 6,
			'options' => ['A' => 'Nướng thịt', 'B' => 'Thái Thịt', 'C' => 'Cho con Bú', 'D' => 'Đấu vật'] 
		],
		[
			'question_id' => 1,
			'options' => ['A' => 1, 'B' => 2, 'C' => 3, 'D' => 4] 
		],       
		[
			'question_id' => 2,
			'options' => ['A' => 'Bỏ cuộc', 'B' => 'Cho tôi qua đi mà', 'C' => 'Không thể qua', 'D' => 'Câu này khó quá'] 
		],
		[
			'question_id' => 3,
			'options' => ['A' => 'Con chim', 'B' => 'Con rắn', 'C' => 'Con người','D' => 'Con tê giác'] 
		],
		[
			'question_id' => 4,
			'options' => ['A' => 'HIV', 'B' => 'Gãy tay', 'C' => 'Siđa', 'D' => 'Bệnh sĩ'] 
		],
		[
			'question_id' => 5,
			'options' => ['A' => 'Hà Nội và Long An', 'B' => 'Hà nội và Quảng Ninh', 'C' => 'Thăng Long và Hạ long', 'D' => 'Quảng Ninh và Long An'] 
		],
		[
			'question_id' => 7,
			'options' => ['A' => 'Ăn Kẹo', 'B' => 'Uống cocacola', 'C' => 'Đá bóng', 'D' => 'Chụp hình'] 
		],
		[
			'question_id' => 8,
			'options' => ['A' => 1,'B' => 2,'C' => 14,'D' => 15] 
		]
	];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>ĐỐ VUI </title>
</head>
<body>
	<h2 style="color: blue; text-align: center;"> Chào mừng bạn đến với cuộc thi </h2>
<!-- B1 : Xuất ra câu hỏi và các đáp án -->
<form action="" method="POST">
	<?php for($i = 1; $i <= count($questions); $i++):?>
		<?php echo 'Câu hỏi '.$i .":";?>
		<?php foreach ($questions as $qu) :?>
			<?php if(($qu['id']) == $i ):?>
				<?php echo $qu['question']."<br>";?>
			<?php endif;?>
		<?php endforeach; ?>
		<?php foreach($options as $traloi) :?>
			<?php if($traloi['question_id'] == $i) :?>
				<input type="radio" name="<?php echo 'ok'.$i;?>" value="A">
				<?php echo 'A. ' .$traloi['options']['A']."<br>";?>
				<input type="radio" name="<?php echo 'ok'.$i; ?>" value="B">
				<?php echo 'B. ' .$traloi['options']['B']."<br>";?>
				<input type="radio" name="<?php echo 'ok'.$i; ?>" value="C">
				<?php echo 'C. ' .$traloi['options']['C']."<br>";?>
				<input type="radio" name="<?php echo 'ok'.$i; ?>" value="D">
				<?php echo 'D. ' .$traloi['options']['D']."<br>";?>
			<?php endif;?>
		<?php endforeach;?>
	<?php endfor;?>
<!--B2 : So sánh đáp án được chọn với Answer , loại bỏ phần tử của mảng unset -->
<div style="text-align: center;">
<input type="submit" name="submit" value="Xem kết quả">
<?php
$dapan =[];
if(isset($_POST['submit'])){
	for($i=1;$i<=count($questions);$i++){
		if(!isset($_POST["ok$i"])){
			$dapan[$i-1] = "khong rep";
		}
		else{
			foreach($answers as $tl){
				if($tl['question_id'] == $i){
					if($tl['option'] == $_POST["ok$i"]){
						$dapan[$i-1] = "CORRECT";
					}
					else{
						$dapan[$i-1] = $tl['option'];
					}
				}
			}
		}
	}
}
?>
<?php if(count($dapan) > 0) :?>
	<?php for($i = 1 ; $i <= count($dapan);$i++) :?>
		<?php if($dapan[$i-1] == 'CORRECT') :?>
			<p style="color: blue;"><?php echo "Câu số " .$i. "Bạn đã đúng" ;?></p>
		<?php endif; ?>
		<?php if($dapan[$i-1] == 'khong rep'):?>
			<p style="color: green;"><?php echo "Trả lời câu số" .$i ." đã nhé" ;?></p>
		<?php endif; ?>
		<?php if($dapan[$i-1] != 'CORRECT' && $dapan[$i-1] != 'khong rep'):?>
			<p style="color: red;"><?php echo 'Chọn '. $_POST["ok$i"] .'là sai nhé !';?></p>
		<?php endif;?>
	<?php endfor;?>
<?php endif;?>

</div>
</form>
</body>
</html>
session_start//luôn ở đầu file 