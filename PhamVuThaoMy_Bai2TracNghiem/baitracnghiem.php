<?php
       session_start();
     
       $diem = 0;
    $answers = [
        ['question_id' => 1, 'option' => 'C'],
        ['question_id' => 2, 'option' => 'A'],
        ['question_id' => 3, 'option' => 'C'],
        ['question_id' => 4, 'option' => 'B'],
        ['question_id' => 5, 'option' => 'C'],
        ['question_id' => 6, 'option' => 'C'],
        ['question_id' => 7, 'option' => 'D'],
        ['question_id' => 8, 'option' => 'D']
    ];

    
    $questions = [
        [
            'id' => 1,
            'question' => 'Có một đàn vịt, cho biết 1 con đi trước thì có 2 con đi sau, 1 con đi sau thì có 2 con đi trước, 1 con đi giữa thì có 2 con đi 2 bên. Hỏi đàn vịt đó có mấy con?'
        ],
        [
            'id' => 2,
            'question' => 'Làm thế nào để qua được câu này?'
        ],
        [
            'id' => 3,
            'question' => 'Sở thú bị cháy, con gì chạy ra đầu tiên?'
        ],
        [
            'id' => 4,
            'question' => 'Bệnh gì bác sỹ bó tay?'
        ],
        [
            'id' => 5,
            'question' => 'Ở Việt Nam, rồng bay ở đâu và đáp ở đâu?'
        ],
        [
            'id' => 6,
            'question' => 'Tay cầm cục thịt nắn nắn, tay vỗ mông là đang làm gì?'
        ],
        [
            'id' => 7,
            'question' => 'Con gấu trúc ao ước gì mà không bao giờ được?'
        ],
        [
            'id' => 8,
            'question' => 'Có 1 đàn chim đậu trên cành, người thợ săn bắn cái rằm. Hỏi chết mấy con?'
        ]
    ];
  
    $options = [
        [
            'question_id' => 6,
            'options' => ['A' => 'Nướng thịt', 'B' => 'Thái Thịt', 'C' => 'Cho con Bú', 'D' => 'Đấu vật'] 
        ],
        [
            'question_id' => 1,
            'options' => ['A' => 1, 'B' => 2, 'C' => 3, 'D' => 4] 
        ],       
        [
            'question_id' => 2,
            'options' => ['A' => 'Bỏ cuộc', 'B' => 'Cho tôi qua đi mà', 'C' => 'Không thể qua', 'D' => 'Câu này khó quá'] 
        ],
        [
            'question_id' => 3,
            'options' => ['A' => 'Con chim', 'B' => 'Con rắn', 'C' => 'Con người','D' => 'Con tê giác'] 
        ],
        [
            'question_id' => 4,
            'options' => ['A' => 'HIV', 'B' => 'Gãy tay', 'C' => 'Siđa', 'D' => 'Bệnh sĩ'] 
        ],
        [
            'question_id' => 5,
            'options' => ['A' => 'Hà Nội và Long An', 'B' => 'Hà nội và Quảng Ninh', 'C' => 'Thăng Long và Hạ long', 'D' => 'Quảng Ninh và Long An'] 
        ],
        [
            'question_id' => 7,
            'options' => ['A' => 'Ăn Kẹo', 'B' => 'Uống cocacola', 'C' => 'Đá bóng', 'D' => 'Chụp hình'] 
        ],
        [
            'question_id' => 8,
            'options' => ['A' => 1,'B' => 2,'C' => 14,'D' => 15] 
        ]
    ];

    


?>

<!DOCTYPE html>
<html>
<head>
    <title>Trắc nghiệm</title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
   <link rel="yylesheet" type="text/css" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
      <div class="all">
         <div class="container">
         
         <div class="head">
             <div class="row">
                   <div class="col-md-9 head1">
                    <h2>Xin chào bạn , mời bạn vào thi</h2>
                   </div>
                   
                   <div class="col-md-3 head2">
                        <img src="<?php echo $_SESSION['user']['avatar']; ?>" style="width: 100px; text-align: center; margin: 60px;"><br>
<p><?php echo $_SESSION['user']['username']; ?>  , Good luck!</p>
                   </div>
             </div>
         </div>
  

        <div class="test">
            <form action="" method="POST">
    
    
    <?php  
        for ($i=0; $i <8 ; $i++) { 
        echo " Câu hỏi số " . ($i + 1) . "  : </b> " .  $questions[$i]['question'] . "<br><br>";
        for ($j=0; $j < 8 ; $j++) { 
            if ($questions[$i]['id'] == $options[$j]['question_id']) {  ?>
                           <input type="radio" name="<?php  echo ($i+1) ?>" value="A" <?php if (isset($_POST['sub'])) {
                             if (isset($_POST[$i+1])) {
                                if ($_POST[$i+1] == 'A') {
                                    echo 'checked';
                                 } 
                             }
                           }   ?>>
                    <prev 

                               <?php 
                                    if (isset($_POST['sub'])) {
                                        if ($answers[$i]['option'] == 'A') {
                                            echo "style ='color : blue'";
                                        }
                                        if (isset($_POST[$i+1]) && $_POST[$i+1] == 'A') {
                                               if ($answers[$i]['option'] != 'A') {
                                                  
                                                   echo "style ='color :red'";
                                                }      
                                               else $diem++;                            
                                        }
                                    }
                                  
                                ?>
                    >       
                    <?php         
                            echo $options[$j]['options']['A'] . "<br>";
                    ?>
                    </prev>
                          <input type="radio" name="<?php  echo ($i+1) ?>" value="B" <?php if (isset($_POST['sub'])) {
                             if (isset($_POST[$i+1])) {
                                if ($_POST[$i+1] == 'B') {
                                    echo 'checked';
                                 } 
                             }
                           }   ?>>  
                    <prev

                                 
                               <?php 
                                    if (isset($_POST['sub'])) {
                                        if ($answers[$i]['option'] == 'B') {
                                            echo "style ='color : blue'";
                                        }
                                        if (isset($_POST[$i+1]) && $_POST[$i+1] == 'B') {
                                               if ($answers[$i]['option'] != 'B') {
                                                  
                                                   echo "style ='color :red'";
                                                }   
                                                else $diem++;                                   
                                        }
                                    }
                                  
                                ?>
                    >       
                    <?php        
                          
                            echo $options[$j]['options']['B'] . "<br>";
                    ?>
                    </prev>
                          <input type="radio" name="<?php  echo ($i+1) ?>" value="C" <?php if (isset($_POST['sub'])) {
                             if (isset($_POST[$i+1])) {
                                if ($_POST[$i+1] == 'C') {
                                    echo 'checked';
                                 } 
                             }
                           }   ?>>
                    <prev
                                   
                               <?php 
                                    if (isset($_POST['sub'])) {
                                        if ($answers[$i]['option'] == 'C') {
                                            echo "style ='color : blue'";
                                        }
                                        if (isset($_POST[$i+1]) && $_POST[$i+1] == 'C') {
                                               if ($answers[$i]['option'] != 'C') {
                                                  
                                                   echo "style ='color :red'";
                                                }   
                                                else $diem++;                                   
                                        }
                                    }
                                  
                                ?>

                    >
                    <?php
                            
                            echo $options[$j]['options']['C'] . "<br>";
                    ?>
                   </prev>
                          <input type="radio" name="<?php echo ($i+1) ?>" value="D" <?php if (isset($_POST['sub'])) {
                             if (isset($_POST[$i+1])) {
                                if ($_POST[$i+1] == 'D') {
                                    echo 'checked';
                                 } 
                             }
                           }   ?>>
                    <prev
                                
                               <?php 
                                    if (isset($_POST['sub'])) {
                                        if ($answers[$i]['option'] == 'D') {
                                            echo "style ='color : blue'";
                                        }
                                        if (isset($_POST[$i+1]) && $_POST[$i+1] == 'D') {
                                               if ($answers[$i]['option'] != 'D') {
                                                  
                                                   echo "style ='color :red'";
                                                }       
                                                else $diem++;                               
                                        }
                                    }
                                  
                                ?>

                    >
                    <?php
                            
                            echo $options[$j]['options']['D'] . "<br>";
                    ?>      

                   </prev>
                   <?php

            }
        }
        
        echo "<br><br><br>";
     }

    ?>

      <div class="nutsub"><input type="submit" name="sub"></div>
  
     </form> 

        

        </div>
               

     </div>
</div>

     <script src="js/jquery-3.2.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>           
</body>
</html>



