<?php 
session_start();
if(!isset($_SESSION['user']['username'])){
	header('Location: index.php');
}
$error = [];
if(isset($_POST['submit'])){
	if($_FILES['avatar']['tmp_name'] != ''){
		$imgFile = getimagesize($_FILES['avatar']['tmp_name']);
		$imgType = strtolower(pathinfo($_FILES['avatar']['tmp_name']));
	}
	if($_FILES['avatar']['error'] != 0 ){
		$error[] = " You don't choose avatar";
	}elseif (!is_array($imgFile)) {
		$error[] = "File không phải là ảnh";
	}elseif($_FILES['avatar']['size'] > 3000000){
		$error[] = 'Ảnh không quá 3 MB';
	}else{
		$save = 'img/' .$_FILES['avatar']['name'];
		$_SESSION['user']['avatar'] = $save;
		move_uploaded_file($_FILES['avatar']['tmp_name'], $save);
		header('Location: baitracnghiem.php');
	}

}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Avatar</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="avar_style.css">
	<link rel="shortcut icon" href="img/logo.png" type="image/vnd.microsoft.icon" id="favicon" />
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light" rel="stylesheet">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/fontawesome-all.css">
	<link rel="stylesheet" href="css/fontawesome-all.min.css">
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
</head>
<body background = "./img/fondo-geometrico-gris_1055-3147.jpg" >
	<div class="ava">
		<div class="container">
			<form action="" method="POST" enctype="multipart/form-data">
				<table background = "./img/28795431_1764230680302502_1253473765914968405_n.jpg" width="100%">
					<tr>
						<td colspan="2">
							<?php if(count($error)>0): ?>
								<?php for($i = 0; $i<count($error);$i++):?>
								<?php echo $error[$i]; ?>
							<?php endfor; ?>
						<?php endif; ?>
						</td>
					</tr>
					<tr>
						<td>Choose file : </td>
						<td><input type="file" name="avatar"></td>
					</tr>
					<tr>
						<td colspan="2"><button type="submit" name="submit">Upload</button></td>
					</tr>
				</table>
			</form>
		</div>
	</div>

</body>
</html>