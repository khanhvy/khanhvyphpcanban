<?php 
session_start();
if(!isset($_SESSION['user']['username'])){
	header('Location: index.php');
}
$error=[];
if(isset($_POST['submit'])){
	if($_FILES['avatar']['tmp_name'] != ''){
		$imgFile = getimagesize($_FILES['avatar']['tmp_name']);
		$imgType = strtolower(pathinfo($_FILES['avatar']['tmp_name']));
	}
	if($_FILES['avatar']['error'] != 0){
		$error[] = "You don't choose avatar";
	}elseif (!is_array($imgFile)) {
		$error[] = "File không phải là ảnh";
	}elseif ($_FILES['avatar']['size'] > 3000000) {
		$error[] = 'Ảnh không quá 3 MB';
	}else{
		$save = 'img/' .$_FILES['avatar']['name'];
		$_SESSION['user']['avatar'] = $save;
		move_uploaded_file($_FILES['avatar']['tmp_name'], $save);
		header('Location: baitracnghiem.php');
	}
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Ảnh đại diện</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="ava">
<form action="" method="POST" enctype="multipart/form-data">
	<table>
		<tr>
			<td colspan="2">
				<?php if(count($error)>0):?>
					<?php for($i=0;$i < count($error) ; $i++) :?>
						<?php echo $error[$i];?>
					<?php endfor; ?>
				<?php endif;?>
			</td>
		</tr>
		<tr>
			<td>Choose avatar: </td>
			<td><input type="file" name="avatar"></td>
		</tr>
		<tr>
			<td colspan="2"><button type="submit" name="submit"> Upload</button></td>
		</tr>
	</table>
</form>
</div>
</body>
</html>

