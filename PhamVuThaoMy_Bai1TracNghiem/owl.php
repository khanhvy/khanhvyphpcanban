<!DOCTYPE html>
<html>
<head>
	<title>OWL</title>
	<link rel="stylesheet" type="text/css" href="/owl/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/owl/css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="/owl/css/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="/owl/css/owl.transitions.css">


    <link href="/owl/css/prettify.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<div id="demo">
      <div class="container">
        <div class="row">
          <div class="span12">

            <div id="owl-demo" class="owl-carousel">
              <div><img src="http://placehold.it/1170x300/42bdc2/FFFFFF"></div>
              <div><img src="http://placehold.it/1170x400/42bdc2/FFFFFF"></div>
              <div><img src="http://placehold.it/1170x500/42bdc2/FFFFFF"></div>
              <div><img src="http://placehold.it/1170x200/42bdc2/FFFFFF"></div>
              <div><img src="http://placehold.it/1170x500/42bdc2/FFFFFF"></div>
              <div><img src="http://placehold.it/1170x250/42bdc2/FFFFFF"></div>
              <div><img src="http://placehold.it/1170x350/42bdc2/FFFFFF"></div>
              <div><img src="http://placehold.it/1170x300/42bdc2/FFFFFF"></div>
              <div><img src="http://placehold.it/1170x100/42bdc2/FFFFFF"></div>
              <div><img src="http://placehold.it/1170x500/42bdc2/FFFFFF"></div>
            </div>

          </div>
        </div>
      </div>
    </div>
	</div>
</body>
<script src="/owl/js/bootstrap.min.js"></script>
<script src="/owl/js/jquery-1.9.1.min.js"></script>
<script src="/owl/js/owl.carousel.min.js"></script>
<script src="/owl/js/bootstrap-collapse.js"></script>
    <script src="/owl/js/bootstrap-transition.js"></script>
    <script src="/owl/js/bootstrap-tab.js"></script>

    <script src="/owl/js/google-code-prettify/prettify.js"></script>
	  <script src="/owl/js/application.js"></script>
	  <script type="text/javascript">
	  	$(document).ready(function() {
      $("#owl-demo").owlCarousel({
        autoPlay : 3000,
        stopOnHover : true,
        navigation:true,
        paginationSpeed : 1000,
        goToFirstSpeed : 2000,
        singleItem : true,
        autoHeight : true,
        transitionStyle:"fade"
      });
    });
	  </script>
</html>
