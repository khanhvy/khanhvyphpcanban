<?php 
require('connect.php');
include("header.php");
$sql = "SELECT * FROM sinhvien WHERE 1=1";

if(isset($_GET['orderbyt']) && $_GET['orderbyt'] != ''){
	$kieu = $_GET['orderbyt'];
	echo $kieu;
}
if(isset($_GET['orderbyc']) && $_GET['orderbyc'] != ''){
	$cot = $_GET['orderbyc'];
	echo $cot;
}
$sql .= " ORDER BY $cot $kieu";
echo $sql;
$query = $db->query($sql);
$result = $query->fetch_all(MYSQLI_ASSOC);
?>
<div class="dssv">
	<div class="container">
		<div class="sapxep" style="margin: 40px;">
		<div class="row">
			<div class="col-sm-4"></div>
			<form action="" method="GET">
			<div class="col-sm-3">Sắp xếp</div>
			<div class="col-sm-2"><select name="orderbyt">
				<option value="ASC" <?php if(isset($_GET['orderbyt']) && $_GET['orderbyt'] == 'ASC') echo 'selected'; ?>>Tăng</option>
				<option value="DESC" <?php if(isset($_GET['orderbyt']) && $_GET['orderbyt'] == 'DESC') echo 'selected';?>>Giảm</option></select></div>
			<div class="col-sm-3"><select name="orderbyc">
				<option value="masv" <?php if(isset($_GET['orderbyc']) && $_GET['orderbyc'] == 'masv') echo 'selected';?>>Mã sv</option>
				<option value="hoten" <?php if(isset($_GET['orderbyc']) && $_GET['orderbyc'] == 'hoten') echo 'selected'; ?>>Họ tên</option>
				<option value="ngaysinh"<?php if(isset($_GET['orderbyc']) && $_GET['orderbyc'] == 'ngaysinh') echo 'selected'; ?>>Ngày sinh</option>
				<option value="makhoa" <?php if(isset($_GET['orderbyc']) && $_GET['orderbyc'] == 'makhoa') echo 'selected'; ?>>Mã khoa</option></select></div>
			<div class="col-sm-2"><input type="submit" name = "sapxep" value= "sapxep">Sắp xếp</div>
			</form>
		</div>          
		</div> 
		<table class="table table-hover">
			<thead>
				<tr>
					<th></th>
					<th>STT</th>
					<th>Mã SV</th>
					<th>Họ tên</th>
					<th>Ngày sinh</th>
					<th>Giới tính</th>
					<th>Địa chỉ</th>
					<th>Email</th>
					<th>Mã khoa</th>

				</tr>
			</thead>
			<tbody>
				<?php
				$stt = 1;
				if(count($result) > 0):
					foreach ($result as $sv):
						?>
						<tr>
							<td><input type="checkbox" name="checksv"></td>
							<td><?php echo $stt++; ?></td>
							<td><?php echo $sv['masv'];?></td>
							<td><?php echo $sv['hoten'];?></td>
							<td><?php echo $sv['ngaysinh'];?></td>
							<td><?php if($sv['gioitinh'] == 1) echo "Nữ"; else{echo "Nam";}?></td>
							<td><?php echo $sv['diachi'];?></td>
							<td><?php echo $sv['email'];?></td>
							<td><?php echo $sv['makhoa'];?></td>
							<td><a href="sinhvien_edit.php">Sửa</a></td>
							<td><a href="sinhvien_edit.php">Xóa</a></td>
						</tr>
					<?php endforeach;?>
				<?php endif;?>
			</tbody>
		</table>
	</div>
</div>
</body>
</html>