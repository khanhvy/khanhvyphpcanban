<?php 
require('connect.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Trang chủ</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/fontawesome-all.css">
	<link rel="stylesheet" href="css/fontawesome-all.min.css">
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>

</head>
<body>
	<div class="header">
		<div class="container">
			<div class="row">
				<div class="col-sm-4" style="background-color:lavender;"><img src="./img/LOGO_DTDH.png" style="width: 80%"></div>
				<div class="col-sm-8" style="background-color:lavenderblush;"><h2>Lớp PHP 1117E2</h2> <h3>Phạm Vũ Thảo My </h3></div>
			</div>
		</div>
	</div>
	<div class="menu">
		<div class="container">
			<h3>Chương trình quản lý sinh viên </h3>
			<ul class="nav nav-tabs">
				<li class="active"><a href="trangchu.php">Trang chủ</a></li>
				<li><a href="sinhvien.php">Sinh viên</a></li>
				<li><a href="#">Menu 3</a></li>
			</ul>
		</div>
	</div>
	<div class="noidung">
		<div class="container">
			<div class="list-group">
				<a href="khoa.php" class="list-group-item list-group-item-success">Tổng số khoa <span class="badge"><?php $sql = "SELECT COUNT(*) FROM khoa"; 
				$query = $db->query($sql);
				$result = $query->fetch_row();
				echo $result[0]; ?></span></a>
				<a href="sinhvien.php" class="list-group-item list-group-item-info">Tổng số Sinh Viên <span class="badge"><?php $sql = "SELECT COUNT(*) FROM sinhvien";
				$query = $db->query($sql);
				$result = $query->fetch_row();
				echo $result[0];?></span></a>
				<a href="monhoc.php" class="list-group-item list-group-item-warning">Tổng số môn học <span class="badge"><?php $sql = "SELECT COUNT(*) FROM mon_hoc";
				$query = $db->query($sql);
				$monhoc = $query->fetch_row();
				echo $monhoc[0];
				$db->close(); ?></span></a>

			</div>
		</div>
	</div>
</body>
</html>
